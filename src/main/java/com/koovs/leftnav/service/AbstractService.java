package com.koovs.leftnav.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.HttpStatus;

import com.koovs.leftnav.domain.AbstractMongoEntity;
import com.koovs.leftnav.exception.BaseException;
import com.koovs.leftnav.exception.EntityNotFoundException;


public abstract class AbstractService<T extends AbstractMongoEntity> implements IGenericService<T> {

  private static final Logger LOGGER = Logger.getLogger(AbstractService.class);


  @Override
  public T findOne(String id) throws EntityNotFoundException {
    T entity = repository().findOne(id);
    if (entity == null) {
      LOGGER.debug("Entity not found : " + id);
      throw notFoundException();
    }
    LOGGER.debug("Entity found : " + entity);
    return entity;
  }

  @Override
  public T save(T domain) {
    domain = repository().save(domain);
    LOGGER.debug("Saved Entity: " + domain);
    return domain;
  }

  @Override
  public List<T> save(List<T> domains) {
    return repository().save(domains);
  }

  @Override
  public Page<T> findAll(Pageable page) {
    Page<T> all = repository().findAll(page);
    LOGGER.debug("Find all paginated: " + page + " Data: " + all);
    return all;
  }

  @Override
  public List<T> findAll() {
    List<T> all = repository().findAll();
    LOGGER.debug("Find all: " + all);
    return all;
  }

  @Override
  public void delete(String id) throws EntityNotFoundException {
    T entity = findOne(id);
    repository().delete(entity);
    LOGGER.debug("Deleted: " + entity);
  }

  @Override
  public T update(String id, T domain) throws EntityNotFoundException {
    LOGGER.debug("Updating id: " + id + " with " + domain);
    T fromDb = findOne(id);
    domain.copyEntityFrom(fromDb);
    return save(domain);
  }

  protected EntityNotFoundException notFoundException() throws EntityNotFoundException {
    throw new EntityNotFoundException();
  }

  protected abstract MongoRepository<T, String> repository();

  public void validatePageAndSize(Integer pageNumber, Integer pageSize) throws BaseException {
    if ((pageNumber == null) || (pageSize == null) || (pageNumber < 0) || (pageSize < 0)) {
      throw new BaseException(HttpStatus.BAD_REQUEST, "PageNumber or PageSize not valid");
    }
  }


}
