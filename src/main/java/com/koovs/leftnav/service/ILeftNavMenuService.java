package com.koovs.leftnav.service;

import java.util.List;

import com.koovs.leftnav.domain.LeftNavMenu;
import com.koovs.leftnav.domain.enums.Platform;
import com.koovs.leftnav.exception.EntityNotFoundException;

public interface ILeftNavMenuService extends IGenericService<LeftNavMenu> {
	List<LeftNavMenu> findByPlatform(Platform platform);
	void approveLeftNavMenu(String id) throws EntityNotFoundException;
}
