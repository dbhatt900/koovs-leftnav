package com.koovs.leftnav.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.koovs.leftnav.domain.LeftNavMenu;
import com.koovs.leftnav.domain.enums.ActiveStatus;
import com.koovs.leftnav.domain.enums.Platform;
import com.koovs.leftnav.exception.EntityNotFoundException;
import com.koovs.leftnav.repository.LeftNavRepository;
import com.koovs.leftnav.service.AbstractService;
import com.koovs.leftnav.service.ILeftNavMenuService;

@Service
public class LeftNavServiceImpl extends AbstractService<LeftNavMenu> implements ILeftNavMenuService {

	@Autowired
	private LeftNavRepository leftNavRepository;

	@Override
	public List<LeftNavMenu> findByPlatform(Platform platform) {
		return leftNavRepository.findByPlatform(platform);
	}

	@Override
	protected MongoRepository<LeftNavMenu, String> repository() {
		return leftNavRepository;
	}

	@Override
	public void approveLeftNavMenu(String id) throws EntityNotFoundException {
		LeftNavMenu leftNavMenu = leftNavRepository.findOne(id);
		if(leftNavMenu != null){
			leftNavMenu.setStatus(ActiveStatus.PENDING);
			update(id, leftNavMenu);
		}
		else {
			throw new EntityNotFoundException();
		}
		
	}

}
