package com.koovs.leftnav.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import com.koovs.leftnav.domain.enums.ActiveStatus;
import com.koovs.leftnav.domain.enums.Platform;

@Document(collection = "leftnavmenu")
public class LeftNavMenu extends AbstractMongoEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8358459250576192095L;

	@NotNull
	@NotEmpty
	private String createdBy;
	
	private String lastUpdatedBy;
	
	@NotNull
	private Date activeFrom;
	
	private ActiveStatus status = ActiveStatus.PENDING_REVIEW;
	
	private Platform platform;
	
	@NotNull
	@Valid
	private List<Data> data = new ArrayList<Data>();

	public LeftNavMenu() {
		super();
	}

	public LeftNavMenu(String createdBy, String lastUpdatedBy, Date activeFrom, ActiveStatus status, Platform platform,
			List<Data> data) {
		super();
		this.createdBy = createdBy;
		this.lastUpdatedBy = lastUpdatedBy;
		this.activeFrom = activeFrom;
		this.status = status;
		this.platform = platform;
		this.data = data;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getActiveFrom() {
		return activeFrom;
	}

	public void setActiveFrom(Date activeFrom) {
		this.activeFrom = activeFrom;
	}

	public ActiveStatus getStatus() {
		return status;
	}

	public void setStatus(ActiveStatus status) {
		this.status = status;
	}

	public Platform getPlatform() {
		return platform;
	}

	public void setPlatform(Platform platform) {
		this.platform = platform;
	}

	public List<Data> getData() {
		return data;
	}

	public void setData(List<Data> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "LeftNavMenu [createdBy=" + createdBy + ", lastUpdatedBy=" + lastUpdatedBy + ", activeFrom=" + activeFrom
				+ ", status=" + status + ", platform=" + platform + "]";
	}
	
	

}
