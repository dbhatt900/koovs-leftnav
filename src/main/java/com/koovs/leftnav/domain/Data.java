package com.koovs.leftnav.domain;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.koovs.leftnav.domain.enums.Action;

public class Data {
	
	private String type;
	
	@NotNull
	@NotBlank
	private String label;
	
	private String href;
	
	private Action action;
	
	@Valid
	private List<Data> data = new ArrayList<Data>();
	
	public Data() {
		super();
	}
	public Data(String type, String label, String href, Action action, List<Data> data) {
		super();
		this.type = type;
		this.label = label;
		this.href = href;
		this.action = action;
		this.data = data;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public Action getAction() {
		return action;
	}
	public void setAction(Action action) {
		this.action = action;
	}
	public List<Data> getData() {
		return data;
	}
	public void setData(List<Data> data) {
		this.data = data;
	}

}
