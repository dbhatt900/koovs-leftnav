package com.koovs.leftnav.domain.enums;

public enum ActiveStatus {
	PENDING_REVIEW,
	PENDING,
	ACTIVE,
	INACTIVE

}
