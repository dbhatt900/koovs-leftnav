package com.koovs.leftnav.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String index() {
		return " Say hello to  koovs left nav api";
	}

}
