package com.koovs.leftnav.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.koovs.leftnav.domain.LeftNavMenu;
import com.koovs.leftnav.domain.enums.Platform;
import com.koovs.leftnav.exception.EntityAlreadyExistsException;
import com.koovs.leftnav.exception.EntityNotFoundException;
import com.koovs.leftnav.service.ILeftNavMenuService;
import com.koovs.leftnav.util.Constant;
import com.koovs.leftnav.util.RestResponse;
import com.koovs.leftnav.util.RestUtils;

@RestController
@RequestMapping(Constant.API_PREFIX + "/left-nav")
public class LeftNavController {

	@Autowired
	private ILeftNavMenuService leftNavMenuService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<RestResponse<LeftNavMenu>> create(@RequestBody @Valid LeftNavMenu leftNavMenu)
			throws  EntityAlreadyExistsException {
		return RestUtils.successResponse(leftNavMenuService.save(leftNavMenu));
	}
	
	@RequestMapping(value = "", method = RequestMethod.PUT)
	public ResponseEntity<RestResponse<LeftNavMenu>> update(@RequestBody @Valid LeftNavMenu leftNavMenu)
			throws  EntityAlreadyExistsException, EntityNotFoundException {
		return RestUtils.successResponse(leftNavMenuService.update(leftNavMenu.getId(), leftNavMenu));
	}
	
	@RequestMapping(value = "/approve/{id}", method = RequestMethod.PUT)
	public ResponseEntity<RestResponse<LeftNavMenu>>approveLeftNavMenu(@PathVariable String id)
			throws  EntityNotFoundException {
		leftNavMenuService.approveLeftNavMenu(id);
		return RestUtils.successResponse(null);
		
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<RestResponse<LeftNavMenu>> getLeftNavmenuById(@PathVariable String id)
			throws EntityNotFoundException {
		
		return RestUtils.successResponse(leftNavMenuService.findOne(id));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<RestResponse<LeftNavMenu>> deleteLeftNav(@PathVariable String id)
			throws EntityNotFoundException {
		leftNavMenuService.delete(id);
		return RestUtils.successResponse(null);
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<RestResponse<List<LeftNavMenu>>> findByPlatform(@RequestParam(value="platform", required=true) String platform)
			throws EntityNotFoundException {
		return RestUtils.successResponse(leftNavMenuService.findByPlatform(Platform.valueOf(platform.toUpperCase())));
	}

}
