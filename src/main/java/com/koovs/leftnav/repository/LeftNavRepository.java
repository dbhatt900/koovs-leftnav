package com.koovs.leftnav.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.koovs.leftnav.domain.LeftNavMenu;
import com.koovs.leftnav.domain.enums.Platform;

public interface LeftNavRepository extends MongoRepository<LeftNavMenu, String> {
	public List<LeftNavMenu> findByPlatform(Platform platform);

}
